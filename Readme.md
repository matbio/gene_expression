# Bulk gene expression for different tissues
## Legend:

- GTex are normal tissues ,
- tumour samples (they are named 'TCGA')
    - 'SKCM' is melanoma', 
    - 'LUAD' and 'LUSC' are two lung cancers.
## Considerations:

gene expression over different samples does not have dramatic variations, but it still changes from sample to sample (note that the distribution of the  standard deviation is  smaller than that of the average, see plots on section 1 of the notebook